import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { GridComponent } from '../grid/component/grid.component';
import { header, SlickGridHeader } from '../grid/value/grid.header';
import { GridOption } from '../grid/value/grid.option';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy, AfterViewInit {
  
  // -------------------------------------------------------------------------------------------------------------------
  // 상수
  // -------------------------------------------------------------------------------------------------------------------
  
  /*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  | Private Variables
  |-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
  
  private rows: any[] = [];
  
  /*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  | Protected Variables
  |-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
  
  @ViewChild('grid')
  protected grid: GridComponent;
  
  /*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  | Public Variables
  |-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
  
  public searchSubject$ = new Subject<string>();
  
  /*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  | Getter & Setter
  |-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
  
  /*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  | Constructor
  |-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
  
  constructor() {
    
    this.searchSubject$
      .debounceTime(300)
      .distinctUntilChanged()
      .switchMap((searchVal) => {
        
        if (this.rows.length > 100000) {
          console.log('e.g) 로딩바를 시작시킨다.');
        }
        
        if (typeof this.grid === 'undefined') {
          return Observable.of<boolean>(false);
        }
        
        return this.grid.search(searchVal);
      })
      .subscribe((statue) => {
        
        if (!statue) {
          console.log('오류 발생 !!!!!!!!!!!!!');
        }
        
        if (this.rows.length > 100000) {
          console.log('e.g) 로딩바를 종료시킨다.');
        }
      });
  }
  
  /*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  | Override Method
  |-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
  
  public ngOnInit(): void {
    
    const headers: header[] = [];
    headers.push(new SlickGridHeader()
      .Id('id')
      .Name('Id')
      .Field('id')
      .Behavior('select')
      .CssClass('cell-selection')
      .Width(100)
      .CannotTriggerInsert(true)
      .Resizable(true)
      .Unselectable(true)
      .Sortable(true)
      .build()
    );
    
    headers.push(new SlickGridHeader()
      .Id('name')
      .Name('Name')
      .Field('name')
      .Behavior('select')
      .CssClass('cell-selection')
      .Width(150)
      .CannotTriggerInsert(true)
      .Resizable(true)
      .Unselectable(true)
      .Sortable(true)
      .build()
    );
    
    headers.push(new SlickGridHeader()
      .Id('age')
      .Name('Age')
      .Field('age')
      .Behavior('select')
      .CssClass('cell-selection')
      .Width(150)
      .CannotTriggerInsert(true)
      .Resizable(true)
      .Unselectable(true)
      .Sortable(true)
      .build()
    );
    
    headers.push(new SlickGridHeader()
      .Id('column1')
      .Name('Column1')
      .Field('column1')
      .Behavior('select')
      .CssClass('cell-selection')
      .Width(200)
      .CannotTriggerInsert(true)
      .Resizable(true)
      .Unselectable(true)
      .Sortable(true)
      .build()
    );
    
    headers.push(new SlickGridHeader()
      .Id('column2')
      .Name('Column2')
      .Field('column2')
      .Behavior('select')
      .CssClass('cell-selection')
      .Width(400)
      .CannotTriggerInsert(true)
      .Resizable(true)
      .Unselectable(true)
      .Sortable(true)
      .build()
    );
    
    headers.push(new SlickGridHeader()
      .Id('column3')
      .Name('Column3')
      .Field('column3')
      .Behavior('select')
      .CssClass('cell-selection')
      .Width(300)
      .CannotTriggerInsert(true)
      .Resizable(true)
      .Unselectable(true)
      .Sortable(true)
      .build()
    );
    
    this.rows = [];
    // noinspection TsLint
    for (let index = 0; index < 10000; index++) {
      const row = (this.rows[index] = {});
      row['name'] = 'name_';
      row['age'] = 'age_';
      row['column1'] = 'column1';
      row['column2'] = 'column2';
      row['column3'] = 'column3';
    }
    
    this.grid.create(headers, this.rows, new GridOption()
      .SyncColumnCellResize(true)
      .MultiColumnSort(true)
      .RowHeight(32)
      .CellExternalCopyManagerActivate(true)
      .DualSelectionActivate(true)
      .build()
    );
  }
  
  public ngOnDestroy() {
  }
  
  public ngAfterViewInit(): void {
  }
  
  /*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  | Public Method
  |-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
  
  /*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  | Protected Method
  |-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
  
  /*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  | Private Method
  |-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
  
}
