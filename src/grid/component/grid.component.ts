import { AfterViewInit, Component, ElementRef, EventEmitter, Output } from '@angular/core';
import { header, SlickGridHeader } from '../value/grid.header';
import { GridOption, Option } from '../value/grid.option';
import { Observable } from 'rxjs/Observable';

declare const jQuery_1_7;
declare const Slick: any;

@Component({
  selector: '[grid-component]',
  templateUrl: './grid.component.html',
  styleUrls: [
    '../../assets/grid/slick.grid.css',         // slickGrid default css
    '../../assets/grid/slick.columnpicker.css', // columnpicker plugin css
    '../../assets/grid/slick.grid.override.css' // slickGrid custom css
  ]
})
export class GridComponent implements AfterViewInit {
  
  /*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
   | Private Variables
   |-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
  
  // Fixed jQuery version 1.7
  private $ = jQuery_1_7;
  // 클릭 이벤트 타임아웃 체크
  private clickEnabled: boolean = true;
  // 정렬 변경 이벤트 타임아웃 체크
  private sortingEnabled: boolean = true;
  
  // -------------------------------------------------------------------------------------------------------------------
  // 상수
  // -------------------------------------------------------------------------------------------------------------------
  
  // 그리드를 생성할 DIV 아이디 PREFIX
  private GRID_TARGET_ID: string = 'myGrid';
  // 그리드에서 사용하는 total 로우 아이디
  private GRID_TOTAL_ID: string = 'total';
  // 로우 데이터가 없는 경우 -1
  private ROW_EMPTY: number = -1;
  
  // -------------------------------------------------------------------------------------------------------------------
  // 그리드 관련 변수
  // -------------------------------------------------------------------------------------------------------------------
  
  private grid;
  private dataView;
  private option: Option;
  private fields: string[] = [];
  private isGridCreated: boolean;
  private isError: boolean;
  private GRID_DEFAULT_OPTION: Option;
  private DATA_VIEW_DEFAULT_OPTION: Object = {
    groupItemMetadataProvider: null,
    inlineFilters: false
  };
  private gridSelectionModelType: string = '';
  
  // 로우 선택시 알림
  @Output() private selectedEvent = new EventEmitter();
  // 정렬 변경시 알림
  @Output() private sortingEvent = new EventEmitter();
  // 헤더 선택시 알림
  @Output() private selectedHeaderEvent = new EventEmitter();
  
  /*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
   | Protected Variables
   |-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
  
  /*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
   | Public Variables
   |-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
  
  /*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
   | Constructor
   |-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
  
  constructor(public elementRef: ElementRef) {
    
    // 그리드 유니크 아이디 생성
    this.createGridUniqueId();
  }
  
  /*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
   | Override Method
   |-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
  
  public ngAfterViewInit(): void {
    
    // 그리드 기본 옵션
    this.GRID_DEFAULT_OPTION = new GridOption()
      .SyncColumnCellResize(true)
      .MultiColumnSort(true)
      .build();
  }
  
  /*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
   | Public Method
   |-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
  
  // -------------------------------------------------------------------------------------------------------------------
  // 데이터 반환 관련
  // -------------------------------------------------------------------------------------------------------------------
  
  /**
   * 데이터뷰 전체 로우 데이터 목록
   * @returns {any[]}
   */
  public getGridRows(): any[] {
    return GridComponent.deepCopy(
      this.dataView.getItems()
    );
  }
  
  /**
   * 데이터뷰에서 선택된 로우 데이터 전체 목록
   * @param scope
   * @returns {any[]}
   */
  public getSelectedRows(scope: any = null): any[] {
    
    const fnScope: any = scope === null ? this : scope;
    
    let rows: any[] =
      fnScope.grid.getSelectedRows()
        .map(rowIndex => fnScope.dataView.getItem(rowIndex));
    
    if (true === fnScope.option.frozenTotal) {
      rows = rows.filter(row => row.id.toString() !== fnScope.GRID_TOTAL_ID);
    }
    
    return GridComponent.deepCopy(rows);
  }
  
  /**
   * 그리드에 보여지고 있는 로우 데이터 목록
   * @param scope
   * @returns {any[]}
   */
  public getRows(scope: any = null): any[] {
    
    const fnScope: any = scope === null ? this : scope;
    
    let rRows: any[] = [];
    
    // 그리드에 보여지고 있는 로우의 숫자
    const gridRowLength = fnScope.dataView.getLength();
    for (let index: number = 0; index < gridRowLength; index += 1) {
      const row: Object = fnScope.dataView.getItem(index);
      if (!('undefined' === typeof row)) {
        rRows.push(row);
      }
    }
    
    if (true === fnScope.option.frozenTotal) {
      rRows = rRows.filter(row => row.id.toString() !== fnScope.GRID_TOTAL_ID);
    }
    
    return GridComponent.deepCopy(rRows);
  }
  
  // -------------------------------------------------------------------------------------------------------------------
  // 선택 관련
  // -------------------------------------------------------------------------------------------------------------------
  
  /**
   * 로우 전체 선택
   * @param scope
   */
  public rowAllSelection(scope: any = null): void {
    
    const fnScope: any = scope === null ? this : scope;
    
    let rRows: any[] = [];
    
    // 그리드에 보여지고 있는 로우의 숫자
    const gridRowLength = fnScope.dataView.getLength();
    for (let index: number = 0; index < gridRowLength; index += 1) {
      rRows.push(index);
    }
    
    if (true === fnScope.option.frozenTotal) {
      
      rRows = rRows.filter((rowIndex) => {
        const row: any = fnScope.dataView.getItem(rowIndex);
        return row.id.toString() !== fnScope.GRID_TOTAL_ID;
      });
    }
    
    fnScope.grid.setSelectedRows(rRows);
  }
  
  /**
   * 로우 전체 선택해제
   * @param scope
   */
  public rowAllUnSelection(scope: any = null): void {
    
    const fnScope: any = scope === null ? this : scope;
    fnScope.grid.setSelectedRows([]);
  }
  
  // -------------------------------------------------------------------------------------------------------------------
  // 레아아웃 관련
  // -------------------------------------------------------------------------------------------------------------------
  
  // noinspection JSUnusedGlobalSymbols
  /**
   * 그리드 높이 변경
   * @param {number} height
   * @param scope
   * @returns {boolean}
   */
  public resizeHeight(height: number, scope: any = null): boolean {
    
    const fnScope: any = scope === null ? this : scope;
    
    try {
      
      fnScope.isGridCreated = false;
      
      // 그리드 아이디로 엘리먼트 선택
      const gridDiv: any = fnScope.$(`#${fnScope.GRID_TARGET_ID}`);
      
      // 그리드를 감싸고 있는 컴포넌트의 높이 값 변경
      this.modifyParentElementHeight(gridDiv, height);
      
      // grid resizeCanvas
      this.grid.resizeCanvas();
    } catch (e) {
      
      fnScope.isGridCreated = true;
      
      // 오류 로그 출력
      console.error(e);
    }
    
    return fnScope.isGridCreated;
  }
  
  // -------------------------------------------------------------------------------------------------------------------
  // 그리드 생성 / 검색 / 추가 / 삭제 / 데이터 변경
  // -------------------------------------------------------------------------------------------------------------------
  
  /**
   * 그리드 생성
   * @param {header[]} headers
   * @param {any[]} rows
   * @param {Option} option
   * @returns {boolean}
   */
  public create(headers: header[], rows: any[], option: Option = null): boolean {
    
    try {
      
      // 아이디 중복나지 않도록 처리
      rows.forEach((row, index) => row._idProperty_ = index);
      
      if (typeof rows[0].id === 'undefined') {
        rows.forEach((row, index) => row.id = index);
      }
      
      this.isGridCreated = false;
      
      // 변수 초기화
      this.fields = [];
      
      // 파라메터 검사
      this.validationParams(headers, option);
      
      // 검색에 사용할 필드이름 목록 생성
      this.createSearchFields(headers);
      
      // DualSelectionActivate 옵션 활성화시
      // 헤더 목록 처음에 아이디 프로퍼트 컬럼의 헤더
      // 데이터 추가
      if (this.option.dualSelectionActivate) {
        headers.unshift(new SlickGridHeader()
          .Id('_idProperty_')
          .Name('')
          .Field('_idProperty_')
          .Behavior('select')
          .CssClass('dual_selection_idProperty')
          .Width(50)
          .CannotTriggerInsert(true)
          .Resizable(false)
          .Unselectable(true)
          .Sortable(false)
          .build()
        );
      }
      
      // 그리드 생성
      this.createGrid(headers, rows);
      
      // 그리드 이벤트 바인딩
      this.bindingGridEvents();
    } catch (e) {
      
      this.isGridCreated = true;
      
      // 오류 로그 출력
      console.error(e);
    }
    
    return this.isGridCreated;
  }
  
  /**
   * 검색
   * @param {string} searchText
   * @param {string[]} searchFields
   * @returns {Observable<boolean>}
   */
  public search(searchText: string = '', searchFields: string[] = []): Observable<boolean> {
    
    try {
      
      this.isError = false;
      
      // 검색에 사용할 필드명 유효성 검사
      this.validationSearchFields(searchFields);
      
      // 선택표시 전체 해제
      this.grid.setSelectedRows([]);
      
      // 검색 실행
      this.executeFilter(searchText, GridComponent.isAllSearch(searchFields) ? this.fields : searchFields);
      
      // dataView refresh
      this.dataView.refresh();
      
      // grid invalidate
      this.grid.invalidate();
    } catch (e) {
      
      this.isError = true;
  
      // 오류 로그 출력
      console.error(e);
    }
    
    return Observable.of<boolean>(this.isError);
  }
  
  /**
   * 데이터뷰에 로우 데이터 추가
   * @param {any[]} row
   * @returns {boolean}
   */
  public addRow(row: any[]): boolean {
    
    try {
      
      this.isError = false;
      
      // 로우 Array 값이 0 인 경우 || 로우 Array 길이가 1 보다 큰 경우
      if (0 === row.length || 1 < row.length) {
        // noinspection ExceptionCaughtLocallyJS
        throw new Error(`Only one array length is allowed. Array length : ${row.length}`);
      }
      
      // 추가할 로우 데이터
      const aRow: any = row[0];
      // 추가할 로우 아이디
      const rowId = aRow.id;
      
      const gridRows: any[] = this.getGridRows();
      if (0 < gridRows.length) {
        // noinspection JSMismatchedCollectionQueryUpdate
        const row: any[] = gridRows.filter(row => String(row.id) === String(rowId));
        
        if (0 < row.length) {
          
          //noinspection ExceptionCaughtLocallyJS
          throw new Error(`An already existing ID has been used. ID: ${rowId}`);
        } else {
          
          // 데이터뷰에 로우 데이터 추가
          gridRows.push(aRow);
          
          // 데이터뷰에 데이터 갱신
          this.dataView.setItems(gridRows);
        }
      } else {
        
        // 데이터뷰에 데이터 갱신
        this.dataView.setItems([aRow]);
      }
      
      // 데이터뷰 refresh
      this.dataView.refresh();
      
      // 그리드 invalidate
      this.grid.invalidate();
    } catch (e) {
      
      this.isError = true;
      
      // 오류 로그 출력
      console.error(e);
    }
    
    return this.isError;
  }
  
  /**
   * 로우 아이디로 로우 삭제
   * @param {string} rowId
   * @returns {boolean}
   */
  public deleteRowByRowId(rowId: string): boolean {
    
    try {
      
      this.isError = false;
      
      // 로우 아이디 값이 있는지 검사
      if (StringUtil.isEmpty(rowId)) {
        // noinspection ExceptionCaughtLocallyJS
        throw new Error(`Row id is empty. ID: ${rowId}`);
      }
      
      // 데이터뷰에서 로우 삭제
      this.dataView.deleteItem(rowId);
      
      // 데이터뷰 로우 갱신
      this.dataView.setItems(this.dataView.getItems());
      
      // dataView refresh
      this.dataView.refresh();
      
      // grid invalidate
      this.grid.invalidate();
    } catch (e) {
      
      this.isError = true;
      
      // 오류 로그 출력
      console.error(e);
    }
    
    return this.isError;
  }
  
  /**
   * 데이터뷰 전체 로우 데이터 변경
   * @param {any[]} rows
   * @returns {boolean}
   */
  public changeRows(rows: any[]): boolean {
    
    try {
      
      this.isError = false;
      
      // 그리드에 Rows 갱신
      this.dataView.setItems(rows);
      
      // 그리드 refresh
      this.dataView.refresh();
      
      // 그리드 invalidate
      this.grid.invalidate();
    } catch (e) {
      
      this.isError = true;
      
      // 오류 로그 출력
      console.error(e);
    }
    
    return this.isError;
  }
  
  // -------------------------------------------------------------------------------------------------------------------
  // 수정 필요.. ( 임시 주석 처리 )
  // -------------------------------------------------------------------------------------------------------------------
  
  // /**
  //  * 로우 아이디 목록을 이용해서 현재 보여지고 있는 그리드 로우 데이터 선택하는 기능
  //  * @param {any[]} rowIds
  //  * @param scope
  //  */
  // public selectedRowByRowIds(rowIds: any[], scope: any = null): void {
  //
  //   const fnScope: any = scope === null ? this : scope;
  //
  //   const eRows: any[] = [];
  //
  //   // 그리드에 보여지고 있는 로우의 숫자
  //   const gridRowLength = fnScope.dataView.getLength();
  //   for (let index: number = 0; index < gridRowLength; index += 1) {
  //
  //     const row: Object = fnScope.dataView.getItem(index);
  //     if (!('undefined' === typeof row)) {
  //       eRows.push(row);
  //     }
  //   }
  //
  //   const rowIndexs: any[] = [];
  //   let index: number = 1;
  //   eRows.forEach((eRow) => {
  //     rowIds.forEach((id) => {
  //       if (eRow.id === id) {
  //         rowIndexs.push(index);
  //       }
  //     });
  //     index += 1;
  //   });
  //
  //   const selectedRows: any[] = fnScope.grid.getSelectedRows();
  //   rowIndexs.forEach(rowIndex => selectedRows.push(fnScope.getRowIndexByRowId(rowIndex)));
  //
  //   fnScope.grid.setSelectedRows(selectedRows);
  // }
  //
  // /**
  //  * 아이디로 선택되어 있는 로우의 선택 해제
  //  * @param rowId
  //  * @param scope
  //  */
  // public unSelectedRowByRowId(rowId: any, scope: any = null): void {
  //
  //   const fnScope: any = scope === null ? this : scope;
  //
  //   fnScope.grid.setSelectedRows(
  //           fnScope.grid.getSelectedRows()
  //                   .filter(rowIndex => fnScope.getRowByRowIndex(rowIndex) !== fnScope.ROW_EMPTY)
  //                   .filter(rowIndex => String(fnScope.getRowByRowIndex(rowIndex).id) !== String(rowId))
  //                   .map(rowIndex => rowIndex)
  //   );
  // }
  
  /*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
   | Protected Method
   |-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
  
  /*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
   | Private Method
   |-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
  
  // -------------------------------------------------------------------------------------------------------------------
  // 그리드 생성에서 사용하는 함수
  // -------------------------------------------------------------------------------------------------------------------
  
  /**
   * 검색에 사용할 필드 목록를 생성한다
   * @param {header[]} headers
   */
  private createSearchFields(headers: header[]): void {
    
    headers.forEach((header) => {
      
      const fieldName: string = header['field'];
      if (!('undefined' === typeof fieldName)) {
        this.fields.push(fieldName);
      }
    });
  }
  
  /**
   * 그리드 생성
   * @param {header[]} headers
   * @param {any[]} rows
   */
  private createGrid(headers: header[], rows: any[]): void {
    
    // 데이터 뷰 생성
    this.dataView = new Slick.Data.DataView(this.DATA_VIEW_DEFAULT_OPTION);
    this.dataView.beginUpdate();
    this.dataView.setItems(rows);
    this.dataView.endUpdate();
    
    this.elementRef.nativeElement.children[0].id = this.GRID_TARGET_ID;
    
    // 그리드 생성
    this.grid = new Slick.Grid(`#${this.GRID_TARGET_ID}`, this.dataView, headers, this.option);
    
    // 툴팁 플러그인 추가
    const autoTooltips = new Slick.AutoTooltips();
    this.grid.registerPlugin(autoTooltips);
    
    // 자동 컬럼 리사이징 플러그인 추가
    const autoSize = new Slick.AutoColumnSize(true);
    this.grid.registerPlugin(autoSize);
    
    // 컬럼 그룹화 플러그인 기능 활성화
    if (true === this.option.columnGroup) {
      const plugin: any = new Slick.ColumnGroup();
      this.grid.registerPlugin(plugin);
    }
    
    if (this.isCellExternalCopyManagerActivate()) {
      
      if (false === this.option.enableCellNavigation) {
        this.option.enableCellNavigation = true;
      }
      
      // 셀 드래그 옵션 초기화
      this.initCellExternalCopyManager(this);
    } else {
      
      // 그리드 로우 선택 기능 활성화
      this.grid.setSelectionModel(
        new Slick.RowSelectionModel({ selectActiveRow: false })
      );
    }
    
    if (true === this.option.frozenTotal) {
      
      const rowsHeight: number = this.getRowsHeight();
      if (rows.length > rowsHeight) {
        this.option.frozenRow = 1;
        this.option.frozenBottom = true;
      } else {
        this.option.frozenRow = -1;
        this.option.frozenBottom = false;
      }
      
      this.grid.setOptions(this.option);
    }
  }
  
  /**
   * 그리드 이벤트 바인딩
   */
  private bindingGridEvents(): void {
    
    // -----------------------------------------------------------------------------------------------------------------
    //  onSort
    //    - sorting 이벤트
    // -----------------------------------------------------------------------------------------------------------------
    
    this.grid.onSort
      .subscribe(
        (function (scope) {
          return function (event, args) {
            try {
              
              if (!scope.sortingEnabled) {
                return;
              }
              
              setTimeout(() => scope.sortingEnabled = true, 300);
              
              scope.sortingEnabled = false;
              
              if (scope.isCellExternalCopyManagerActivate()) {
                scope.grid.setSelectedRows([]);
              } else {
                // 정렬 변경시에는 선택 표시 유지를 위해서 sync
                scope.dataView.syncGridSelection(scope.grid, true);
              }
              
              let totalRow: any = null;
              
              if (true === scope.option.frozenTotal) {
                
                const rowsHeight: number = this.getRowsHeight();
                
                if (scope.dataView.getLength() > rowsHeight) {
                  scope.option['frozenRow'] = 1;
                  scope.option['frozenBottom'] = true;
                } else {
                  scope.option['frozenRow'] = -1;
                  scope.option['frozenBottom'] = false;
                }
                
                scope.grid.setOptions(scope.option);
                scope.grid.invalidate();
                
                totalRow = GridComponent.deepCopy(
                  scope.getRowByRowIndex(scope.getRowIndexByRowId(scope.GRID_TOTAL_ID))
                );
                
                if (!(-1 === totalRow)) {
                  scope.deleteRowByRowId(scope.GRID_TOTAL_ID);
                }
              }
              
              const cols = args.sortCols;
              scope.dataView.sort((row1, row2) => {
                for (let index: number = 0; index < cols.length; index += 1) {
                  const field = cols[index].sortCol.field;
                  const sign = cols[index].sortAsc ? 1 : -1;
                  const value1 = row1[field];
                  const value2 = row2[field];
                  const result = (value1 === value2 ? 0 : (value1 > value2 ? 1 : -1)) * sign;
                  if (!(0 === result)) {
                    return result;
                  }
                }
                return 0;
              });
              
              if (true === scope.option.frozenTotal) {
                if (!(null === totalRow) && !(-1 === totalRow)) {
                  scope.addRow([totalRow]);
                  scope.dataView.getItemMetadata(scope.getRows().length - 1);
                }
              }
  
              scope.grid.invalidate();
              scope.grid.render();
              
              scope.clickGridCanvasElement(scope);
            } catch (e) {
              console.error(e);
            } finally {
              scope.sortingEvent.emit();
            }
          };
        })(this)
      );
    
    // -----------------------------------------------------------------------------------------------------------------
    //  onClick
    //    - 클릭 이벤트
    // -----------------------------------------------------------------------------------------------------------------
    
    if (!this.isCellExternalCopyManagerActivate() || this.option.dualSelectionActivate) {
      this.grid.onClick.subscribe(
        (function (scope) {
          return function (event, args) {
            
            if (!scope.clickEnabled) {
              return;
            }
            
            setTimeout(() => scope.clickEnabled = true, 300);
            
            scope.clickEnabled = false;
            
            //noinspection JSValidateJSDoc
            /**
             * 1. 선택된 로우 데이터
             * 2. 선택 여부
             * 3. 이벤트 오브젝트
             * 4. 에러 여부
             *
             * @type {{row: any; selected: any; event: any; error: boolean}}
             */
            const result = {
              event,
              row: null,
              selected: null,
              error: false
            };
            
            try {
              
              // 로우 인덱스 값
              const rowIndex: number = args.row;
              
              // 로우 인덱스로 로우 데이터 추출
              const row: any = scope.dataView.getItem(rowIndex);
              
              // 로우 데이터가 없으면
              if (row === scope.ROW_EMPTY) {
                //noinspection ExceptionCaughtLocallyJS
                throw Error('Row is empty.');
              }
              
              if (scope.option.dualSelectionActivate) {
                if (args.cell === 0) {
                  
                  if (scope.gridSelectionModelType === 'cell') {
                    
                    scope.gridSelectionModelType = 'row';
                    
                    // 전체 선택 해제
                    scope.rowAllUnSelection();
                  }
                  
                  // 그리드 로우 선택 기능 활성화
                  scope.initRowSelectionModel(scope);
                  
                  scope.onClickRowSelection(scope, row, result, rowIndex);
                } else {
                  
                  scope.gridSelectionModelType = 'cell';
                  
                  // 전체 선택 해제
                  scope.rowAllUnSelection();
                  
                  // 셀 드래그 옵션 초기화
                  scope.initCellExternalCopyManager(scope);
                }
              } else {
                
                scope.onClickRowSelection(scope, row, result, rowIndex);
              }
              
              result.row = row;
              scope.grid.invalidate();
              scope.grid.render();
            } catch (e) {
              
              // 에러 발생 여부
              result.error = true;
              
              // 에러 정보 출력
              console.error(e);
            } finally {
              
              // 클릭 이벤트 발생
              scope.selectedEvent.emit(result);
            }
          };
        })(this)
      );
    }
    
    // -----------------------------------------------------------------------------------------------------------------
    //  onRowCountChanged
    //    - DataView의 Row 숫자가 변경되면 발생하는 이벤트
    // -----------------------------------------------------------------------------------------------------------------
    
    this.dataView.onRowCountChanged.subscribe(
      (function (scope) {
        return function () {
          
          if (true === scope.option.frozenTotal) {
            
            const rowsHeight: number = this.getRowsHeight();
            
            if (scope.dataView.getLength() > rowsHeight) {
              scope.option.frozenRow = 1;
              scope.option.frozenBottom = true;
            } else {
              scope.option.frozenRow = -1;
              scope.option.frozenBottom = false;
            }
            
            scope.grid.setOptions(scope.option);
            scope.grid.invalidate();
          }
          
          scope.grid.updateRowCount();
          scope.grid.render();
        };
      })(this)
    );
    
    // -----------------------------------------------------------------------------------------------------------------
    //  onHeaderClick
    //    - 헤더 클릭 이벤트
    // -----------------------------------------------------------------------------------------------------------------
    
    if (this.option.enableHeaderClick) {
      this.grid.onHeaderClick.subscribe(
        (function (scope) {
          return function (event, args) {
            const columnId = args.column.id;
            const columnIdx = scope.grid.getColumnIndex(columnId);
            scope.selectedHeaderEvent.emit(columnId);
            
            for (let idx = 0; idx < scope.grid.getDataLength(); idx = idx + 1) {
              const $cell = scope.$(scope.grid.getCellNode(idx, columnIdx));
              if ($cell.hasClass('selected')) {
                $cell.removeClass('selected');
              } else {
                $cell.addClass('selected');
              }
            }
            
          };
        })(this)
      );
    }
    
    // -----------------------------------------------------------------------------------------------------------------
    //  DualSelectionActivate 옵션이 활성화 된 경우
    // -----------------------------------------------------------------------------------------------------------------
    
    if (this.option.dualSelectionActivate) {
      
      // -------------------------------
      // 헤더 클릭시
      // -------------------------------
      
      this.grid.onHeaderClick.subscribe(
        (function (scope) {
          return function (event, args) {
            
            // 헤더가 아이디 프로퍼티 필드인 경우
            if (scope.isHeaderFieldIdProperty(args)) {
              
              if (scope.getSelectedRows().length > 0) {
                scope.rowAllUnSelection();
              } else {
                scope.rowAllSelection();
              }
              
              // 그리드의 캔버스 엘리먼트 클릭 처리
              scope.clickGridCanvasElement(scope);
            }
          };
        })(this)
      );
    }
    
  }
  
  private initRowSelectionModel(scope) {
    scope.grid.setSelectionModel(
      new Slick.RowSelectionModel({ selectActiveRow: false })
    );
  }

// -------------------------------------------------------------------------------------------------------------------
  // 그리드 검색에서 사용하는 함수
  // -------------------------------------------------------------------------------------------------------------------
  
  /**
   * 전체 필드에서 검색이라면
   * @param {string[]} searchFields
   * @returns {boolean}
   */
  private static isAllSearch(searchFields: string[]): boolean {
    return searchFields.length === 0;
  }
  
  /**
   * 검색 실행
   * @param {string} searchText
   * @param {string[]} fields
   */
  private executeFilter(searchText: string, fields: string[]): void {
    
    if (StringUtil.isEmpty(searchText) && 0 === searchText.length) {
      
      this.dataView.setFilter(() => {
        return true;
      });
    } else {
      
      this.dataView.setFilter((row) => {
        
        let isValue = false;
        fields.forEach((key) => {
          
          if (-1 < String(row[key]).toLocaleUpperCase().indexOf(searchText.toLocaleUpperCase())) {
            isValue = true;
          }
          
          if (this.GRID_TOTAL_ID === row.id) {
            isValue = this.option.frozenTotal === true;
          }
        });
        return isValue;
      });
    }
  }
  
  /**
   * 검색 필드 유효성 검사
   *  - 생성시점에 만들어진 헤더의 필드정보랑 비교 검색하려는 필드 값이 헤더에 없는 필드명이면 예외가 발생
   * @param {string[]} fields
   */
  private validationSearchFields(fields: string[]): void {
    
    fields.forEach((argSearchField) => {
      
      let isCheckedField = false;
      this.fields.forEach((field) => {
        if (argSearchField === field) {
          isCheckedField = true;
        }
      });
      
      if (!isCheckedField) {
        throw new Error(`Not found '${argSearchField}' field name.`);
      }
    });
  }
  
  // -------------------------------------------------------------------------------------------------------------------
  // 그리드 내부에서 로우 오브젝트를 찾기위한 함수 ( 인덱스 | 로우 아이디를 이용 )
  // -------------------------------------------------------------------------------------------------------------------
  
  /**
   * 데이터뷰 데이터에서 로우 인덱스로 로우 데이터 추출
   * @param {number} rowIndex
   * @param scope
   * @returns {any | number}
   */
  private getRowByRowIndex(rowIndex: number, scope: any = null): any | number {
    const fnScope: any = scope === null ? this : scope;
    const fnRowIndex: number = typeof rowIndex !== 'number' ? Number(rowIndex) : rowIndex;
    const row: Object = fnScope.dataView.getItemByIdx(fnRowIndex);
    return typeof row === 'undefined' ? fnScope.ROW_EMPTY : row;
  }
  
  /**
   * 데이터뷰에서 로우의 아이디로 로우의 인덱스를 구한다
   * @param rowId
   * @returns {any | number}
   */
  private getRowIndexByRowId(rowId: any): any | number {
    const rowIndex: number = this.dataView.getIdxById(rowId);
    return typeof rowIndex === 'undefined' ? this.ROW_EMPTY : rowIndex;
  }
  
  // -------------------------------------------------------------------------------------------------------------------
  // 그리드 검색에서 사용하는 함수
  // -------------------------------------------------------------------------------------------------------------------
  
  /**
   * 그리드 유니크 아이디 생성
   */
  private createGridUniqueId(): void {
    
    const date: Date = new Date();
    const gridTargetIdSuffix: string =
      date.getFullYear().toString() +
      (date.getMonth() + 1).toString() +
      date.getDate().toString() +
      date.getMinutes().toString() +
      date.getMilliseconds().toString();
    this.GRID_TARGET_ID = this.GRID_TARGET_ID + gridTargetIdSuffix;
  }
  
  /**
   * @returns {number}
   */
  private getRowsHeight(): number {
    return Math.floor(this.$(this.GRID_TARGET_ID)
      .find('.slick-pane-top').height() / this.option.rowHeight);
  }
  
  // -------------------------------------------------------------------------------------------------------------------
  // 그리드 옵션 체크를 위한 함수
  // -------------------------------------------------------------------------------------------------------------------
  
  /**
   * 셀 복사 붙여넣기 기능 활성화 여부 검사
   * @returns {boolean}
   */
  private isCellExternalCopyManagerActivate(): boolean {
    return true === this.option.cellExternalCopyManagerActivate;
  }
  
  // -------------------------------------------------------------------------------------------------------------------
  //
  // -------------------------------------------------------------------------------------------------------------------
  
  /**
   * DeepCopy
   * @param {T} o
   * @returns {T}
   */
  private static deepCopy<T>(o: T): T {
    return JSON.parse(JSON.stringify(o));
  }
  
  //noinspection JSMethodCanBeStatic
  /**
   * 그리드를 감싸고 있는 컴포넌트의 높이 값 변경
   * @param gridDiv
   * @param {number} height
   */
  private modifyParentElementHeight(gridDiv: any, height: number): void {
    gridDiv.parent().css({ height: height + 'px' });
  }
  
  /**
   * 그리드를 만들기 전에 파라메터 유효성 검사
   * @param {header[]} headers
   * @param {Option} option
   */
  private validationParams(headers: header[], option: Option): void {
    
    // Header 검사
    if (0 === headers.length) {
      
      throw new TypeError('Invalid header value.');
    }
    
    // 옵션 검사
    if (!(null === option)) {
      this.option = JSON.stringify(option) === '{}' ? this.GRID_DEFAULT_OPTION : option;
    } else {
      this.option = this.GRID_DEFAULT_OPTION;
    }
  }
  
  /**
   * 셀 드래그 옵션 초기화
   * @param scope
   */
  private initCellExternalCopyManager(scope): void {
    
    scope.grid.setSelectionModel(
      new Slick.CellSelectionModel({ selectActiveRow: false })
    );
    
    const cellCopyManager = new Slick.CellExternalCopyManager();
    cellCopyManager.init(scope.grid);
  }
  
  /**
   * 클릭 이벤트 > 로우 선택 표시
   * @param scope
   * @param row
   * @param {{event: any; row: any; selected: any; error: boolean}} result
   * @param {number} rowIndex
   */
  private onClickRowSelection(scope: any, row: any, result: { event: any; row: any; selected: any; error: boolean }, rowIndex: number): void {
    
    const hasRow: any[] = scope.getSelectedRows()
      .filter(selectedRow => String(selectedRow.id) === String(row.id));
    
    result.selected = hasRow.length <= 0;
    if (result.selected) {
      const selectedRows: any[] = scope.grid.getSelectedRows();
      selectedRows.push(rowIndex);
      
      scope.grid.setSelectedRows(selectedRows);
    } else {
      let selectedRows: any[] = scope.grid.getSelectedRows();
      selectedRows = selectedRows
        .filter(selectedRowIndex => String(selectedRowIndex) !== String(rowIndex));
      
      scope.grid.setSelectedRows(selectedRows);
    }
  }
  
  /**
   * 헤더 필드가 아이디 프로퍼티인 경우
   * @param args
   * @returns {boolean}
   */
  private isHeaderFieldIdProperty(args): boolean {
    return args.column.field === '_idProperty_';
  }
  
  /**
   * 그리드의 캔버스 엘리먼트 클릭 처리
   * @param scope
   */
  private clickGridCanvasElement(scope): void {
    scope.elementRef.nativeElement
      .querySelector('.slick-viewport')
      .querySelector('.grid-canvas')
      .click();
  }
  
  /**
   *
   * @param e
   * @returns {boolean}
   */
  private isIdPropertyAreaDrag(e): boolean {
    return e.target.className.indexOf('dual_selection_idProperty') === -1;
  }
  
}

// ---------------------------------------------------------------------------------------------------------------------
// 컴포넌트 내부에서 사용할 유틸리티 클래스
// ---------------------------------------------------------------------------------------------------------------------

export class StringUtil {
  
  /**
   * 파라미터가 비어있는지 여부
   * param : 문자열
   * @param {string} param
   * @returns {boolean}
   */
  public static isEmpty(param: string): boolean {
    
    // 빈값일때
    return param === undefined || param === null || (typeof param !== 'number' && this.trim(param) === '');
  }
  
  /**
   * trim 처리
   * param : 문자열
   * @param {string} param
   * @returns {string}
   */
  public static trim(param: string): string {
    
    return param.trim();
  }
  
}
