/**
 * slick grid header builder class
 */
export class SlickGridHeader {
  
  /* tslint:disable:variable-name function-name */
  private _asyncPostRender: string = null;
  private _behavior: string = null;
  private _cannotTriggerInsert: boolean = null;
  private _cssClass: string = '';
  private _defaultSortAsc: boolean = true;
  private _editor: string = null;
  private _field: string = '';
  private _focusable: boolean = true;
  private _formatter: string = null;
  private _headerCssClass: string = null;
  private _id: string = '';
  private _maxWidth: string = null;
  private _minWidth: number = 30;
  private _name: string = '';
  private _rerenderOnResize: boolean = false;
  private _resizable: boolean = true;
  private _selectable: boolean = true;
  private _sortable: boolean = false;
  private _toolTip: string = '';
  private _width: number;

  ///////////////////////////////////////////////////////////
  //
  // _validator
  // _unselectable
  //
  // - 원본 라이브러리에 없는 옵션
  //
  ///////////////////////////////////////////////////////////

  private _validator: any;
  private _unselectable: boolean;

  constructor() {
  }

  get asyncPostRender(): string {
    return this._asyncPostRender;
  }

  //noinspection JSUnusedGlobalSymbols
  AsyncPostRender(value: string): SlickGridHeader {
    this._asyncPostRender = value;
    return this;
  }

  get behavior(): string {
    return this._behavior;
  }

  //noinspection JSUnusedGlobalSymbols
  Behavior(value: string): SlickGridHeader {
    this._behavior = value;
    return this;
  }

  get cannotTriggerInsert(): boolean {
    return this._cannotTriggerInsert;
  }

  //noinspection JSUnusedGlobalSymbols
  CannotTriggerInsert(value: boolean): SlickGridHeader {
    this._cannotTriggerInsert = value;
    return this;
  }

  get cssClass(): string {
    return this._cssClass;
  }

  //noinspection JSUnusedGlobalSymbols
  CssClass(value: string): SlickGridHeader {
    this._cssClass = value;
    return this;
  }

  get defaultSortAsc(): boolean {
    return this._defaultSortAsc;
  }

  //noinspection JSUnusedGlobalSymbols
  DefaultSortAsc(value: boolean): SlickGridHeader {
    this._defaultSortAsc = value;
    return this;
  }

  get editor(): string {
    return this._editor;
  }

  //noinspection JSUnusedGlobalSymbols
  Editor(value: string): SlickGridHeader {
    this._editor = value;
    return this;
  }

  get field(): string {
    return this._field;
  }

  //noinspection JSUnusedGlobalSymbols
  Field(value: string): SlickGridHeader {
    this._field = value;
    return this;
  }

  get focusable(): boolean {
    return this._focusable;
  }

  //noinspection JSUnusedGlobalSymbols
  Focusable(value: boolean): SlickGridHeader {
    this._focusable = value;
    return this;
  }

  get formatter(): string {
    return this._formatter;
  }

  //noinspection JSUnusedGlobalSymbols
  Formatter(value: string): SlickGridHeader {
    this._formatter = value;
    return this;
  }

  get headerCssClass(): string {
    return this._headerCssClass;
  }

  //noinspection JSUnusedGlobalSymbols
  HeaderCssClass(value: string): SlickGridHeader {
    this._headerCssClass = value;
    return this;
  }

  get id(): string {
    return this._id;
  }

  //noinspection JSUnusedGlobalSymbols
  Id(value: string): SlickGridHeader {
    this._id = value;
    return this;
  }

  get maxWidth(): string {
    return this._maxWidth;
  }

  //noinspection JSUnusedGlobalSymbols
  MaxWidth(value: string): SlickGridHeader {
    this._maxWidth = value;
    return this;
  }

  get minWidth(): number {
    return this._minWidth;
  }

  //noinspection JSUnusedGlobalSymbols
  MinWidth(value: number): SlickGridHeader {
    this._minWidth = value;
    return this;
  }

  get name(): string {
    return this._name;
  }

  //noinspection JSUnusedGlobalSymbols
  Name(value: string): SlickGridHeader {
    this._name = value;
    return this;
  }

  get rerenderOnResize(): boolean {
    return this._rerenderOnResize;
  }

  //noinspection JSUnusedGlobalSymbols
  RerenderOnResize(value: boolean): SlickGridHeader {
    this._rerenderOnResize = value;
    return this;
  }

  get resizable(): boolean {
    return this._resizable;
  }

  //noinspection JSUnusedGlobalSymbols
  Resizable(value: boolean): SlickGridHeader {
    this._resizable = value;
    return this;
  }

  get selectable(): boolean {
    return this._selectable;
  }

  //noinspection JSUnusedGlobalSymbols
  Selectable(value: boolean): SlickGridHeader {
    this._selectable = value;
    return this;
  }

  get sortable(): boolean {
    return this._sortable;
  }

  //noinspection JSUnusedGlobalSymbols
  Sortable(value: boolean): SlickGridHeader {
    this._sortable = value;
    return this;
  }

  get toolTip(): string {
    return this._toolTip;
  }

  //noinspection JSUnusedGlobalSymbols
  ToolTip(value: string): SlickGridHeader {
    this._toolTip = value;
    return this;
  }

  get width(): number {
    return this._width;
  }

  //noinspection JSUnusedGlobalSymbols
  Width(value: number): SlickGridHeader {
    this._width = value;
    return this;
  }

  get validator(): any {
    return this._validator;
  }

  //noinspection JSUnusedGlobalSymbols
  Validator(value: any): SlickGridHeader {
    this._validator = value;
    return this;
  }

  get unselectable(): boolean {
    return this._unselectable;
  }

  //noinspection JSUnusedGlobalSymbols
  Unselectable(value: boolean): SlickGridHeader {
    this._unselectable = value;
    return this;
  }

  build(): header {
    return new header(this);
  }

}

export class header {

  private asyncPostRender: string;
  private behavior: string;
  private cannotTriggerInsert: boolean;
  private cssClass: string;
  private defaultSortAsc: boolean;
  private editor: string;
  private field: string;
  private focusable: boolean;
  private formatter: string;
  private headerCssClass: string;
  private id: string;
  private maxWidth: string;
  private minWidth: number;
  private name: string;
  private rerenderOnResize: boolean;
  private resizable: boolean;
  private selectable: boolean;
  private sortable: boolean;
  private toolTip: string;
  private width: number;
  private validator: any;
  private unselectable: boolean;

  constructor(builder: SlickGridHeader) {
    if (typeof builder.asyncPostRender !== 'undefined') {
      this.asyncPostRender = builder.asyncPostRender;
    }
    if (typeof builder.behavior !== 'undefined') {
      this.behavior = builder.behavior;
    }
    if (typeof builder.cannotTriggerInsert !== 'undefined') {
      this.cannotTriggerInsert = builder.cannotTriggerInsert;
    }
    if (typeof builder.cssClass !== 'undefined') {
      this.cssClass = builder.cssClass;
    }
    if (typeof builder.defaultSortAsc !== 'undefined') {
      this.defaultSortAsc = builder.defaultSortAsc;
    }
    if (typeof builder.editor !== 'undefined') {
      this.editor = builder.editor;
    }
    if (typeof builder.field !== 'undefined') {
      this.field = builder.field;
    }
    if (typeof builder.focusable !== 'undefined') {
      this.focusable = builder.focusable;
    }
    if (typeof builder.formatter !== 'undefined') {
      this.formatter = builder.formatter;
    }
    if (typeof builder.headerCssClass !== 'undefined') {
      this.headerCssClass = builder.headerCssClass;
    }
    if (typeof builder.id !== 'undefined') {
      this.id = builder.id;
    }
    if (typeof builder.maxWidth !== 'undefined') {
      this.maxWidth = builder.maxWidth;
    }
    if (typeof builder.minWidth !== 'undefined') {
      this.minWidth = builder.minWidth;
    }
    if (typeof builder.name !== 'undefined') {
      this.name = builder.name;
    }
    if (typeof builder.rerenderOnResize !== 'undefined') {
      this.rerenderOnResize = builder.rerenderOnResize;
    }
    if (typeof builder.resizable !== 'undefined') {
      this.resizable = builder.resizable;
    }
    if (typeof builder.selectable !== 'undefined') {
      this.selectable = builder.selectable;
    }
    if (typeof builder.sortable !== 'undefined') {
      this.sortable = builder.sortable;
    }
    if (typeof builder.toolTip !== 'undefined') {
      this.toolTip = builder.toolTip;
    }
    if (typeof builder.width !== 'undefined') {
      this.width = builder.width;
    }
    if (typeof builder.validator !== 'undefined') {
      this.validator = builder.validator;
    }
    if (typeof builder.unselectable !== 'undefined') {
      this.unselectable = builder.unselectable;
    }
  }
  /* tslint:enable:variable-name function-name */
}
